unit PesquisaFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBClient, Grids, DBGrids, StdCtrls, Menus, DBCtrls, dbcgrids,
  ComCtrls, ImgList;

type
  TfrmPesquisa = class(TForm)
    dsItensMenu: TDataSource;
    cdsTodos: TClientDataSet;
    cdsTodosName: TStringField;
    cdsTodosCaption: TStringField;
    cdsTodosCaptionPesquisa: TStringField;
    cdsTodosCaminho: TStringField;
    cdsTodosCaminhoPesquisa: TStringField;
    tcPrincipal: TTabControl;
    edtPesquisa: TEdit;
    dbctrlgrdResultado: TDBCtrlGrid;
    dbtxtCaption: TDBText;
    dbtxtCaminho: TDBText;
    imlPrincipal: TImageList;
    cdsRecentes: TClientDataSet;
    cdsFavoritos: TClientDataSet;
    pmTodos: TPopupMenu;
    miAdicionarFavoritos: TMenuItem;
    cdsRecentesName: TStringField;
    cdsRecentesCaption: TStringField;
    cdsRecentesCaptionPesquisa: TStringField;
    cdsRecentesCaminho: TStringField;
    cdsRecentesCaminhoPesquisa: TStringField;
    cdsFavoritosName: TStringField;
    cdsFavoritosCaption: TStringField;
    cdsFavoritosCaptionPesquisa: TStringField;
    cdsFavoritosCaminho: TStringField;
    cdsFavoritosCaminhoPesquisa: TStringField;
    miRemoverItem: TMenuItem;
    miRemoverTodos: TMenuItem;
    cdsPreferencias: TClientDataSet;
    cdsPreferenciasChave: TStringField;
    cdsPreferenciasValor: TStringField;
    cdsTodosOrdem: TIntegerField;
    cdsRecentesOrdem: TIntegerField;
    cdsFavoritosOrdem: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edtPesquisaChange(Sender: TObject);
    procedure edtPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtPesquisaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbctrlgrdResultadoDblClick(Sender: TObject);
    procedure dbctrlgrdResultadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbctrlgrdResultadoPaintPanel(DBCtrlGrid: TDBCtrlGrid;
      Index: Integer);
    procedure miAdicionarFavoritosClick(Sender: TObject);
    procedure tcPrincipalChange(Sender: TObject);
    procedure pmTodosPopup(Sender: TObject);
    procedure miRemoverItemClick(Sender: TObject);
    procedure miRemoverTodosClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbctrlgrdResultadoMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    FMainMenu: TMainMenu;
    FTitulo: string;
    FConfirmou: Boolean;
    ListaItens: TStringList;
    Caminho: string;
    ItemEventoExecutar: TMenuItem;
    FCorItemDestaque: TColor;
    FSeparadorMenu: string;
    NomeArquivoRecentes: string;
    NomeArquivoFavoritos: string;
    NomeArquivoPreferencias: string;
    OrdemItem: Integer;
    procedure SetMainMenu(const Value: TMainMenu);
    procedure CarregarListaItensMenu;
    procedure CarregarRecentes;
    procedure CarregarFavoritos;
    procedure CriarItem(Item: TMenuItem);
    procedure PesquisarItem;
    procedure FiltrarRegistros;
    procedure ExecutarMenu;
    procedure SetTitulo(const Value: string);
    procedure AjustarAltura;
    procedure SetCorItemDestaque(const Value: TColor);
    procedure SetSeparadorMenu(const Value: string);
    function AdaptarTextoPesquisa(const StrCaption: string): string;
    function RemoverCaracteresEspeciais(const Texto: string): string;
    procedure AtualizarQtdResultados;
    procedure AdicionarItemAFavoritos;
    procedure AjustarAbaSelecionada;
    procedure SalvarFavoritos;
    procedure SalvarRecentes;
    procedure SalvarPreferencias;
    procedure RemoverTodosItens;
    procedure AdicionarItemARecentes;
    procedure CarregarPreferencias;
    function ObterProximaOrdem(DataSet: TClientDataSet): Integer;
    procedure CopiarRegistro(DataSet: TClientDataSet);
    procedure ObterNomeArquivos;
    function ObterDiretorioTemporario: string;
    procedure StrResetLength(var S: AnsiString);
  public
    property MainMenu: TMainMenu read FMainMenu write SetMainMenu;
    property Titulo: string read FTitulo write SetTitulo;
    property Confirmou: Boolean read FConfirmou;
    property CorItemDestaque: TColor read FCorItemDestaque write SetCorItemDestaque;
    property SeparadorMenu: string read FSeparadorMenu write SetSeparadorMenu;
    procedure ExecutarEventoSelecionado;
  end;

implementation

const
  COR_EDIT_VAZIO          = clWhite;
  COR_ITEM_ENCONTRADO     = $00ECFFEC;
  COR_ITEM_NAO_ENCONTRADO = $00D2D2FF;

  ALTURA_MAXIMA    = 572;
  ALTURA_ITEM      = 35;
  MARGEM_SUPERIOR  = 86;

  QTD_MAXIMA_ITENS = 14;

  CHAVE_ULTIMA_ABA_SELECIONADA = 'ULTIMA_ABA_SELECIONADA';

{$R *.dfm}

{ TfrmPesquisa }

procedure TfrmPesquisa.CarregarFavoritos;
begin
  cdsFavoritos.CreateDataSet;
  if FileExists(NomeArquivoFavoritos) then
    cdsFavoritos.LoadFromFile(NomeArquivoFavoritos);
end;

procedure TfrmPesquisa.CarregarListaItensMenu;
const
  MSG_INICIAL = 'Digite o valor a pesquisar...';
var
  i: Integer;
begin
  cdsTodos.CreateDataSet;

  cdsTodos.DisableControls;
  try
    OrdemItem := 0;
    for i := 0 to FMainMenu.Items.Count - 1 do
    begin
      Caminho := StringReplace(TrimLeft(FMainMenu.Items[i].Caption), '&', EmptyStr, [rfReplaceAll]) + FSeparadorMenu;
      CriarItem(FMainMenu.Items[i]);
    end;

    cdsTodos.First;

    edtPesquisa.Hint := MSG_INICIAL;
    edtPesquisa.SelectAll;

    AjustarAltura;
  finally
    cdsTodos.EnableControls;
  end;
end;

procedure TfrmPesquisa.CarregarPreferencias;
begin
  cdsPreferencias.CreateDataSet;
  if FileExists(NomeArquivoPreferencias) then
    cdsPreferencias.LoadFromFile(NomeArquivoPreferencias);

  if cdsPreferencias.Locate('Chave', CHAVE_ULTIMA_ABA_SELECIONADA, [loCaseInsensitive]) then
    tcPrincipal.TabIndex := cdsPreferenciasValor.AsInteger;
end;

procedure TfrmPesquisa.CarregarRecentes;
begin
  cdsRecentes.CreateDataSet;
  if FileExists(NomeArquivoRecentes) then
    cdsRecentes.LoadFromFile(NomeArquivoRecentes);
end;

procedure TfrmPesquisa.CriarItem(Item: TMenuItem);
var
  j: Integer;
  CaminhoAnterior: string;
begin
  if Assigned(Item.OnClick) and (Item.Caption <> '-') and (Item.Visible) and (Item.Enabled) then
  begin
    ListaItens.AddObject(Item.Name, Item);

    cdsTodos.Insert;
    cdsTodosOrdem.AsInteger := OrdemItem;
    cdsTodosName.AsString := Item.Name;
    cdsTodosCaption.AsString := TrimLeft(StringReplace(Item.Caption, '&', EmptyStr, [rfReplaceAll]));
    cdsTodosCaptionPesquisa.AsString := AdaptarTextoPesquisa(cdsTodosCaption.AsString);

    Caminho := Copy(Caminho, 1, Length(Caminho) - Length(FSeparadorMenu));
    cdsTodosCaminho.AsString := Caminho;
    cdsTodosCaminhoPesquisa.AsString := AdaptarTextoPesquisa(cdsTodosCaminho.AsString);

    cdsTodos.Post;
    Inc(OrdemItem);
  end;

  for j := 0 to Item.Count - 1 do
  begin
    CaminhoAnterior := Caminho;
    Caminho := Caminho + StringReplace(TrimLeft(Item[j].Caption), '&', EmptyStr, [rfReplaceAll]) + FSeparadorMenu;
    CriarItem(Item[j]);
    Caminho := CaminhoAnterior;
  end;
end;

procedure TfrmPesquisa.ExecutarMenu;
var
  Indice: Integer;
begin
  Indice := ListaItens.IndexOf(dsItensMenu.DataSet.FieldByName('Name').AsString);

  if Indice = -1 then
    Exit;

  ItemEventoExecutar := TMenuItem(ListaItens.Objects[Indice]);
  FConfirmou := True;

  Close;
end;

procedure TfrmPesquisa.FiltrarRegistros;
const
  FILTRO_NENHUM_REGISTRO = '1 = 2';
  LIMITE_CONDICOES = 252;
var
  TextoPesquisar, Filtro: string;
  QtdCondicoes: Integer;
begin
  if (edtPesquisa.Text = EmptyStr) then
  begin
    dsItensMenu.DataSet.Filtered := False;
    Exit;
  end;

  TextoPesquisar := AdaptarTextoPesquisa(edtPesquisa.Text);

  {N�o foi poss�vel implementar filtro parcial utilizando propriedade "Filter" do DataSet.
   Por algum motivo desconhecido, "like" n�o funciona dos dois lados. Por esse motivo,
   foi implementado da forma abaixo. N�o � a mais "elegante", no entanto foi a forma encontrada.
   Marcelo Sim�es, 20-ago-2012, 16:56h}
  Filtro := EmptyStr;
  QtdCondicoes := 0;
  dsItensMenu.DataSet.Filtered := False;
  dsItensMenu.DataSet.First;
  while not dsItensMenu.DataSet.Eof do
  begin
    if (Pos(TextoPesquisar, dsItensMenu.DataSet.FieldByName('CaptionPesquisa').AsString) > 0) or
       (Pos(TextoPesquisar, dsItensMenu.DataSet.FieldByName('CaminhoPesquisa').AsString) > 0) then
    begin
      Filtro := Filtro + 'OR (Ordem=' + dsItensMenu.DataSet.FieldByName('Ordem').AsString + ') ';
      Inc(QtdCondicoes);
    end;

    {Testes demonstraram que h� um limite de condi��es a ser utilizado no "Filter", portanto
     foi utilizado o controle abaixo para evitar erros.
     Marcelo Sim�es, 20-ago-2012, 16:56h}
    if QtdCondicoes >= LIMITE_CONDICOES then
      Break;

    dsItensMenu.DataSet.Next;
  end;

  if Filtro <> EmptyStr then
    Delete(Filtro, 1, 3)
  else
    Filtro := FILTRO_NENHUM_REGISTRO;

  dsItensMenu.DataSet.Filter := Filtro;
  dsItensMenu.DataSet.Filtered := True;
end;

procedure TfrmPesquisa.PesquisarItem;
begin
  dsItensMenu.DataSet.DisableControls;
  try
    if edtPesquisa.Text = EmptyStr then
    begin
      dsItensMenu.DataSet.Filtered := False;
      dsItensMenu.DataSet.First;
      edtPesquisa.Color := COR_EDIT_VAZIO;
      Exit;
    end;

    FiltrarRegistros;

    if not dsItensMenu.DataSet.IsEmpty then
      edtPesquisa.Color := COR_ITEM_ENCONTRADO
    else
      edtPesquisa.Color := COR_ITEM_NAO_ENCONTRADO;
  finally
    AjustarAltura;
    AtualizarQtdResultados;
    dsItensMenu.DataSet.EnableControls;
  end;
end;

procedure TfrmPesquisa.pmTodosPopup(Sender: TObject);
begin
  miRemoverItem.Visible := dsItensMenu.DataSet <> cdsTodos;
  miRemoverTodos.Visible := dsItensMenu.DataSet <> cdsTodos;
  miAdicionarFavoritos.Visible := dsItensMenu.DataSet <> cdsFavoritos;
end;

procedure TfrmPesquisa.SetMainMenu(const Value: TMainMenu);
begin
  FMainMenu := Value;
end;

procedure TfrmPesquisa.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SalvarPreferencias;
end;

procedure TfrmPesquisa.FormCreate(Sender: TObject);
begin
  ListaItens := TStringList.Create;
  FConfirmou := False;
  Caminho := EmptyStr;
  Self.Constraints.MaxHeight := ALTURA_MAXIMA;
end;

procedure TfrmPesquisa.FormDestroy(Sender: TObject);
begin
  ListaItens.Free;
end;

procedure TfrmPesquisa.edtPesquisaChange(Sender: TObject);
begin
  PesquisarItem;
end;

procedure TfrmPesquisa.edtPesquisaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_DOWN: dsItensMenu.DataSet.Next;
    VK_UP: dsItensMenu.DataSet.Prior;
    VK_RETURN: ExecutarMenu;
  end;
end;

procedure TfrmPesquisa.edtPesquisaClick(Sender: TObject);
begin
  if edtPesquisa.SelLength = 0 then
    edtPesquisa.SelectAll;
end;

procedure TfrmPesquisa.FormActivate(Sender: TObject);
begin
  ObterNomeArquivos;

  CarregarListaItensMenu;
  CarregarRecentes;
  CarregarFavoritos;
  CarregarPreferencias;

  AjustarAbaSelecionada;
end;

procedure TfrmPesquisa.SetTitulo(const Value: string);
begin
  FTitulo := Value;
  Self.Caption := FTitulo;
end;

procedure TfrmPesquisa.tcPrincipalChange(Sender: TObject);
begin
  AjustarAbaSelecionada;
end;

procedure TfrmPesquisa.AjustarAbaSelecionada;
begin
  case tcPrincipal.TabIndex of
    0: dsItensMenu.DataSet := cdsTodos;
    1: dsItensMenu.DataSet := cdsRecentes;
    2: dsItensMenu.DataSet := cdsFavoritos;
  end;

  PesquisarItem;
end;

procedure TfrmPesquisa.ExecutarEventoSelecionado;
begin
  ItemEventoExecutar.OnClick(ItemEventoExecutar);
  AdicionarItemARecentes;
end;

procedure TfrmPesquisa.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_ESCAPE: Close;
  end;

  if (Shift = [ssCtrl]) then
  begin
    case Key of
      VK_PRIOR:
        begin
          if tcPrincipal.TabIndex > 0 then
          begin
            tcPrincipal.TabIndex := tcPrincipal.TabIndex - 1;
            AjustarAbaSelecionada;
          end;
        end;
      VK_NEXT:
        begin
          if tcPrincipal.TabIndex < tcPrincipal.Tabs.Count then
          begin
            tcPrincipal.TabIndex := tcPrincipal.TabIndex + 1;
            AjustarAbaSelecionada;
          end;
        end;
    end;
  end;

end;

procedure TfrmPesquisa.miAdicionarFavoritosClick(Sender: TObject);
begin
  AdicionarItemAFavoritos;
end;

procedure TfrmPesquisa.AdicionarItemAFavoritos;
begin
  if cdsFavoritos.Locate('Name', dsItensMenu.DataSet.FieldByName('Name').Value, [loCaseInsensitive]) then
    Exit;

  CopiarRegistro(cdsFavoritos);

  SalvarFavoritos;
end;

procedure TfrmPesquisa.CopiarRegistro(DataSet: TClientDataSet);
begin
  DataSet.Insert;
  DataSet.FieldByName('Ordem').AsInteger := ObterProximaOrdem(DataSet);
  DataSet.FieldByName('Name').Value := dsItensMenu.DataSet.FieldByName('Name').Value;
  DataSet.FieldByName('Caption').Value := dsItensMenu.DataSet.FieldByName('Caption').Value;
  DataSet.FieldByName('CaptionPesquisa').Value := dsItensMenu.DataSet.FieldByName('CaptionPesquisa').Value;
  DataSet.FieldByName('Caminho').Value := dsItensMenu.DataSet.FieldByName('Caminho').Value;
  DataSet.FieldByName('CaminhoPesquisa').Value := dsItensMenu.DataSet.FieldByName('CaminhoPesquisa').Value;
  DataSet.Post;
end;

procedure TfrmPesquisa.ObterNomeArquivos;
const
  NOME_DIR = '%s\MSPesquisaMenu';
  MASCARA_NOME_ARQ_RECENTES = '%s\%s_Recentes.xml';
  MASCARA_NOME_ARQ_FAVORITOS = '%s\%s_Favoritos.xml';
  MASCARA_NOME_ARQ_PREFERENCIAS = '%s\%s_Preferencias.xml';
var
  NomeAplicacao, Diretorio: string;
begin
  NomeAplicacao := ChangeFileExt(ExtractFileName(Application.ExeName), EmptyStr);
  Diretorio := Format(NOME_DIR, [ObterDiretorioTemporario]);

  if not DirectoryExists(Diretorio) then
    CreateDir(Diretorio);

  NomeArquivoRecentes := Format(MASCARA_NOME_ARQ_RECENTES, [Diretorio, NomeAplicacao]);
  NomeArquivoFavoritos := Format(MASCARA_NOME_ARQ_FAVORITOS, [Diretorio, NomeAplicacao]);
  NomeArquivoPreferencias := Format(MASCARA_NOME_ARQ_PREFERENCIAS, [Diretorio, NomeAplicacao]);
end;

procedure TfrmPesquisa.StrResetLength(var S: AnsiString);
var
  i: Integer;
begin
  for i := 1 to Length(S) do
  begin
    if S[i] = #0 then
    begin
      S := Copy(S, 1, i - 1);
      Exit;
    end;
  end;
end;

function TfrmPesquisa.ObterDiretorioTemporario: string;
var
  BufSize: Cardinal;
  Diretorio: string;
  DiretorioAux: AnsiString;
begin
  BufSize := Windows.GetTempPath(0, nil);
  SetLength(Diretorio, BufSize);
  Windows.GetTempPath(BufSize, PChar(Diretorio));
  DiretorioAux := AnsiString(Diretorio);
  StrResetLength(DiretorioAux);
  Result := string(DiretorioAux);
end;

function TfrmPesquisa.ObterProximaOrdem(DataSet: TClientDataSet): Integer;
var
  DataSetClone: TClientDataSet;
begin
  DataSetClone := TClientDataSet.Create(Self);
  try
    DataSetClone.CloneCursor(DataSet, True);
    DataSetClone.IndexFieldNames := 'Ordem';
    DataSetClone.Last;
    Result := DataSetClone.FieldByName('Ordem').AsInteger + 1;
  finally
    DataSetClone.Close;
    DataSetClone.Free;
  end;
end;

procedure TfrmPesquisa.AdicionarItemARecentes;
begin
  if cdsRecentes.Locate('Name', dsItensMenu.DataSet.FieldByName('Name').Value, [loCaseInsensitive]) then
  begin
    {Altera a ordem para ficar em primeiro da lista}
    cdsRecentes.Edit;
    cdsRecentes.FieldByName('Ordem').AsInteger := ObterProximaOrdem(cdsRecentes);
    cdsRecentes.Post;
  end
  else
  begin
    {Apaga �ltimo item para inserir o mais recente}
    if cdsRecentes.RecordCount = QTD_MAXIMA_ITENS then
    begin
      cdsRecentes.Last;
      cdsRecentes.Delete;
    end;

    CopiarRegistro(cdsRecentes);
  end;

  SalvarRecentes;
end;

procedure TfrmPesquisa.AjustarAltura;
var
  Altura: Integer;
begin
  Altura := MARGEM_SUPERIOR + (dsItensMenu.DataSet.RecordCount * ALTURA_ITEM);

  if Altura < ALTURA_MAXIMA then
    Self.Height := Altura
  else
    Self.Height := ALTURA_MAXIMA;

  if dsItensMenu.DataSet.RecordCount < QTD_MAXIMA_ITENS then
  begin
    dbctrlgrdResultado.RowCount := dsItensMenu.DataSet.RecordCount;
    dbctrlgrdResultado.Height := dsItensMenu.DataSet.RecordCount * ALTURA_ITEM;
  end
  else
  begin
    dbctrlgrdResultado.RowCount := QTD_MAXIMA_ITENS;
    dbctrlgrdResultado.Height := QTD_MAXIMA_ITENS * ALTURA_ITEM;
  end;

end;

procedure TfrmPesquisa.SalvarFavoritos;
begin
  cdsFavoritos.SaveToFile(NomeArquivoFavoritos);
end;

procedure TfrmPesquisa.SalvarPreferencias;
begin
  if not cdsPreferencias.Locate('Chave', CHAVE_ULTIMA_ABA_SELECIONADA, [loCaseInsensitive]) then
  begin
    cdsPreferencias.Insert;
    cdsPreferenciasChave.AsString := CHAVE_ULTIMA_ABA_SELECIONADA;
  end
  else
    cdsPreferencias.Edit;

  cdsPreferenciasValor.AsInteger := tcPrincipal.TabIndex;
  cdsPreferencias.Post;

  cdsPreferencias.SaveToFile(NomeArquivoPreferencias);
end;

procedure TfrmPesquisa.SalvarRecentes;
begin
  cdsRecentes.SaveToFile(NomeArquivoRecentes);
end;

procedure TfrmPesquisa.SetCorItemDestaque(const Value: TColor);
begin
  FCorItemDestaque := Value;
  dbctrlgrdResultado.SelectedColor := FCorItemDestaque;
end;

procedure TfrmPesquisa.dbctrlgrdResultadoDblClick(Sender: TObject);
begin
  ExecutarMenu;
end;

procedure TfrmPesquisa.dbctrlgrdResultadoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN: ExecutarMenu;
  end;
end;

procedure TfrmPesquisa.dbctrlgrdResultadoMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  {Ao clicar com bot�o direito do mouse em um item que n�o est� previamente selecionado,
   o evento abaixo simula clique do bot�o esquerdo do mouse, para selecionar o item clicado.}
  if Button = mbRight then
    Mouse_Event(MOUSEEVENTF_ABSOLUTE or MOUSEEVENTF_LEFTDOWN, X, Y, 0, 0);
end;

procedure TfrmPesquisa.dbctrlgrdResultadoPaintPanel(
  DBCtrlGrid: TDBCtrlGrid; Index: Integer);
begin
  dbtxtCaminho.Hint := dbtxtCaminho.Caption;
end;

procedure TfrmPesquisa.SetSeparadorMenu(const Value: string);
begin
  FSeparadorMenu := Value;
end;

function TfrmPesquisa.AdaptarTextoPesquisa(const StrCaption: string): string;
begin
  Result := TrimLeft(RemoverCaracteresEspeciais(UpperCase(StrCaption)));
end;

function TfrmPesquisa.RemoverCaracteresEspeciais(const Texto : string): string;
const
  TOTAL_CARACTERES = 38;
  LISTA_CARACTERES_ESPECIAIS: array[1..TOTAL_CARACTERES] of string =
    ('�', '�', '�', '�', '�','�', '�', '�', '�', '�', '�', '�','�', '�','�', '�','�', '�', '�',
     '�', '�','�', '�','�', '�', '�', '�', '�', '�', '�', '�','�','�', '�','�','�','�','�');
  LISTA_CARACTERES_SUBSTITUIR: array[1..TOTAL_CARACTERES] of string =
    ('A', 'A', 'A', 'A', 'A','A', 'A', 'A', 'A', 'A', 'E', 'E','E', 'E','I', 'I','I', 'I', 'O',
     'O', 'O','O', 'O','O', 'O', 'O', 'O', 'O', 'U', 'U', 'U','U','U', 'U','C','C','N', 'N');
var
  Str : string;
  i : Integer;
begin
   Str := Texto;

   for i := 1 to TOTAL_CARACTERES do
     Str := StringReplace(Str, LISTA_CARACTERES_ESPECIAIS[i], LISTA_CARACTERES_SUBSTITUIR[i], [rfReplaceAll]);

   Result := Str;
end;


procedure TfrmPesquisa.miRemoverItemClick(Sender: TObject);
begin
  TClientDataSet(dsItensMenu.DataSet).Delete;

  if dsItensMenu.DataSet = cdsRecentes then
    SalvarRecentes
  else
    SalvarFavoritos;

  PesquisarItem;
end;

procedure TfrmPesquisa.miRemoverTodosClick(Sender: TObject);
begin
  RemoverTodosItens;
end;

procedure TfrmPesquisa.RemoverTodosItens;
begin
  TClientDataSet(dsItensMenu.DataSet).EmptyDataSet;

  if dsItensMenu.DataSet = cdsRecentes then
    SalvarRecentes
  else
    SalvarFavoritos;

  PesquisarItem;
end;

procedure TfrmPesquisa.AtualizarQtdResultados;
begin
  Caption := Format('%s (%d resultado(s))', [FTitulo, dsItensMenu.DataSet.RecordCount]);
end;

end.
