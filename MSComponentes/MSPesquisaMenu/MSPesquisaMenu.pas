{
  TODO:
    - Criar propriedade para definir atalho para chamar pesquisa;
}

unit MSPesquisaMenu;

interface

uses
  SysUtils, Classes, Forms, Menus, Graphics;

type
  TMSPesquisaMenu = class(TComponent)
  private
    FMainMenu: TMainMenu;
    FTituloTelaPesquisa: string;
    FCorItemDestaque: TColor;
    FSeparadorMenu: string;
    FAtalho: TShortCut;
    procedure SetMainMenu(const Value: TMainMenu);
    procedure SetTituloTelaPesquisa(const Value: string);
    procedure SetCorItemDestaque(const Value: TColor);
    procedure SetSeparadorMenu(const Value: string);
    procedure SetAtalho(const Value: TShortCut);
  protected
  public
    procedure Executar;
    constructor Create(AOwner: TComponent); override;
  published
    property MainMenu: TMainMenu read FMainMenu write SetMainMenu;
    property TituloTelaPesquisa: string read FTituloTelaPesquisa write SetTituloTelaPesquisa;
    property CorItemDestaque: TColor read FCorItemDestaque write SetCorItemDestaque;
    property SeparadorMenu: string read FSeparadorMenu write SetSeparadorMenu;
    property Atalho: TShortCut read FAtalho write SetAtalho;
  end;

procedure Register;

resourcestring
  SMsgErroMainMenuNaoInformado = 'Para executar a pesquisa, � necess�rio informar a propriedade "MainMenu" ' +
    'no componente "%s".';

implementation

uses PesquisaFrm;

const
  COR_ITEM_DESTAQUE = $00FFE8E8;
  SEPARADOR_MENU_PADRAO = ' \ ';

procedure Register;
begin
  RegisterComponents('MS Componentes', [TMSPesquisaMenu]);
end;

{ TMSPesquisaMenu }

procedure TMSPesquisaMenu.Executar;
var
  frmPesquisa: TfrmPesquisa;
begin
  Assert(Assigned(FMainMenu), Format(SMsgErroMainMenuNaoInformado, [Name]));

  frmPesquisa := TfrmPesquisa.Create(Application.MainForm);
  try
    frmPesquisa.MainMenu := FMainMenu;
    frmPesquisa.Titulo := FTituloTelaPesquisa;
    frmPesquisa.CorItemDestaque := FCorItemDestaque;
    frmPesquisa.SeparadorMenu := FSeparadorMenu;
    frmPesquisa.ShowModal;
    if frmPesquisa.Confirmou then
      frmPesquisa.ExecutarEventoSelecionado;
  finally
    FreeAndNil(frmPesquisa);
  end;
end;

procedure TMSPesquisaMenu.SetTituloTelaPesquisa(const Value: string);
begin
  FTituloTelaPesquisa := Value;
end;

procedure TMSPesquisaMenu.SetMainMenu(const Value: TMainMenu);
begin
  FMainMenu := Value;
end;

procedure TMSPesquisaMenu.SetCorItemDestaque(const Value: TColor);
begin
  FCorItemDestaque := Value;
end;

constructor TMSPesquisaMenu.Create(AOwner: TComponent);
begin
  FCorItemDestaque := COR_ITEM_DESTAQUE;
  FSeparadorMenu := SEPARADOR_MENU_PADRAO;
  inherited Create(AOwner);
end;

procedure TMSPesquisaMenu.SetSeparadorMenu(const Value: string);
begin
  FSeparadorMenu := Value;
end;

procedure TMSPesquisaMenu.SetAtalho(const Value: TShortCut);
begin
  FAtalho := Value;
end;

end.
 