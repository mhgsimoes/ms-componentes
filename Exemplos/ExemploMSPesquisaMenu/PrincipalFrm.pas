unit PrincipalFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, MSPesquisaMenu, StdCtrls;

type
  TfrmExemplo = class(TForm)
    MSPesquisaMenu: TMSPesquisaMenu;
    mmPrincipal: TMainMenu;
    Sistema1: TMenuItem;
    Fechar1: TMenuItem;
    Cadastros1: TMenuItem;
    Cliente1: TMenuItem;
    Fornecedor1: TMenuItem;
    Usurio1: TMenuItem;
    Categoria1: TMenuItem;
    Produto1: TMenuItem;
    Empresa1: TMenuItem;
    Ajuda1: TMenuItem;
    Sobre1: TMenuItem;
    Help1: TMenuItem;
    abeladepreos1: TMenuItem;
    Estoque1: TMenuItem;
    N1: TMenuItem;
    Entrada1: TMenuItem;
    Sada1: TMenuItem;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MenuClick(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Sobre1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmExemplo: TfrmExemplo;

implementation

{$R *.dfm}

procedure TfrmExemplo.MenuClick(Sender: TObject);
begin
  MessageDlg(Format('Clicou no menu: %s', [TMenuItem(Sender).Caption]), mtInformation, [mbOk], 0);
end;

procedure TfrmExemplo.Fechar1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmExemplo.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (ssCtrl in Shift) and (Key = VK_SPACE) then
    MSPesquisaMenu.Executar;
end;

procedure TfrmExemplo.Sobre1Click(Sender: TObject);
resourcestring
  SMsgSobre =
    'Componente desenvolvido por Marcelo Sim�es.' + #13#13 +
    'Contato: mhgs11@gmail.com';
begin
  MessageDlg(SMsgSobre, mtInformation, [mbOk], 0);
end;

end.
