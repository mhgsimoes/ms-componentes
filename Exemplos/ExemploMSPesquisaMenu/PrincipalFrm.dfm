object frmExemplo: TfrmExemplo
  Left = 0
  Top = 0
  Caption = 
    'Exemplo de utiliza'#231#227'o do MSPesquisaMenu - Desenvolvido por Marce' +
    'lo Sim'#245'es (mhgs11@gmail.com)'
  ClientHeight = 419
  ClientWidth = 690
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Menu = mmPrincipal
  OldCreateOrder = False
  Position = poMainFormCenter
  WindowState = wsMaximized
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 352
    Height = 16
    Caption = '1) Pressione Ctrl + ESPA'#199'O para exibir a pesquisa de menus.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 32
    Width = 424
    Height = 16
    Caption = 
      '2) Use "Ctrl + Page Up" e "Ctrl + Page Down" para alternar entre' +
      ' as abas.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 56
    Width = 475
    Height = 16
    Caption = 
      '3) Clique com o bot'#227'o direito do mouse sobre os itens para acess' +
      'ar op'#231#245'es extras.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 16
    Top = 96
    Width = 546
    Height = 16
    Caption = 
      '*Observa'#231#227'o: Apenas os itens que tiverem algum evento associado ' +
      'ser'#227'o listados na pesquisa.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object MSPesquisaMenu: TMSPesquisaMenu
    MainMenu = mmPrincipal
    CorItemDestaque = 16771304
    SeparadorMenu = ' \ '
    Atalho = 0
    Left = 200
    Top = 184
  end
  object mmPrincipal: TMainMenu
    Left = 88
    Top = 184
    object Sistema1: TMenuItem
      Caption = 'Sistema'
      object Estoque1: TMenuItem
        Caption = 'Estoque'
        object Entrada1: TMenuItem
          Caption = 'Entrada'
          OnClick = MenuClick
        end
        object Sada1: TMenuItem
          Caption = 'Sa'#237'da'
          OnClick = MenuClick
        end
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Fechar1: TMenuItem
        Caption = 'Fechar'
        OnClick = Fechar1Click
      end
    end
    object Cadastros1: TMenuItem
      Caption = 'Cadastros'
      object Cliente1: TMenuItem
        Caption = 'Cliente'
        OnClick = MenuClick
      end
      object Fornecedor1: TMenuItem
        Caption = 'Fornecedor'
        OnClick = MenuClick
      end
      object Usurio1: TMenuItem
        Caption = 'Usu'#225'rio'
        OnClick = MenuClick
      end
      object Categoria1: TMenuItem
        Caption = 'Categoria'
        OnClick = MenuClick
      end
      object Produto1: TMenuItem
        Caption = 'Produto'
        OnClick = MenuClick
      end
      object Empresa1: TMenuItem
        Caption = 'Empresa'
        OnClick = MenuClick
      end
      object abeladepreos1: TMenuItem
        Caption = 'Tabela de pre'#231'os'
        OnClick = MenuClick
      end
    end
    object Ajuda1: TMenuItem
      Caption = 'Ajuda'
      object Sobre1: TMenuItem
        Caption = 'Sobre'
        OnClick = Sobre1Click
      end
      object Help1: TMenuItem
        Caption = 'Help'
        OnClick = MenuClick
      end
    end
  end
end
